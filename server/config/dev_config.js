//Para configurar entornos de ejecución en una aplicación node se utiliza la
// variable global process, que es propia de cualquier app de node
//En este caso el archivo contiene las variables para el entorno de desarrollo

// ==============
// Definir Puerto
//===============

process.env.PORT = process.env.PORT || 3000 //EL "||" es similar al "??" de c#, y es que si no está definido el valor que queremos asignarle a una variable, se utiliza otro por defecto