require("./config/dev_config"); // Cargar variables de entorno

const express = require('express')
const bparser = require("body-parser") //<-- Paquete/líbrería node que permite parsear el body o paidload del request en un objeto deserializado

const app = express()

// parse application/x-www-form-urlencoded | Para deserializar un paidload x-www-form-urlencoded
app.use(bparser.urlencoded({ extended: false }))

// parse application/json   |  para deserializar un body json
app.use(bparser.json())

//app.use define un middlewares (funciones que se disparan cada vez que se llegue a la línea donde se define uno)
//cada vez que se llama a un endpoint del servicio, se ejecutan esos middle ware

app.get('/usuario', function(req, res) {
    res.json('Get Usuario')
})

app.post('/usuario', function(req, res) {

    let reqBody = req.body //Body pre-procesado por el middleware body-parser

    if (reqBody.nombre === undefined) {
        res.status(400).json({
                Mensaje: "Ups, algo salió mal",
                ok: false
            }) //Retornar un estado http a la request y objeto de response
    } else {
        res.json({
            reqBody: reqBody
        })
    }
})

app.put('/usuario/:id', function(req, res) { // ":id" con ":" simboliza que esa parte de la url es un parámetro, similar a c# cuando se especifica {id} en el httpmethod

    let id_param = req.params.id //obtener el parametro "id" de la URL
    res.json({
        id: id_param
    })
})

app.put('/usuario', function(req, res) {
    res.json('Delete Usuario')
})

app.get('/', function(req, res) {
    res.json('Hello World')
})

app.listen(process.env.PORT, () => { // Se utiliza el puerto definido en el archivo de configuración para el entorno deseado
    console.log("Servidor iniciado en puerto 3000");
})